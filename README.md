# README #

A simple utility to convert [iterm2](http://iterm2.com/) color schemes to [roxterm](http://roxterm.sourceforge.net/) format.
Given limited number of color schemes for Roxterm, you might find it useful if you work on OSX and Linux platforms.


### How do I use this? ###

After locating the .xml file of the color scheme you want to convert, run the follwoing:

```python itemr2roxterm.py /path/to/iterm2/color_scheme.xml```

Upon completion, you should see a file with the same name (no extension) as the original iterm2 file.
Put the new file in Roxterm's *Colour* folder:
```~/.config/roxterm.sourceforge.net/Colours```

### Contribution guidelines ###

* Writing tests
* Adding support to convert to other popular file formats
