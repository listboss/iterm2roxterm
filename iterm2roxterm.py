import os
import plistlib
import re
import math


def writeRoxtermColor(colorSet, outFilename):
    with open(outFilename, 'wt') as fp:
        fp.write('[roxterm colour scheme]\n')
        for k in sorted(colorSet.keys()):
            r = '%02x' % colorSet[k][0]
            g = '%02x' % colorSet[k][1]
            b = '%02x' % colorSet[k][2]
            hexcol = '#{0}{0}{1}{1}{2}{2}'.format(r, g, b)
            print('{0}={1}'.format(k, hexcol))
            fp.write('{0}={1}\n'.format(k, hexcol))
        fp.write('palette_size={0}'.format(len(colorSet)))


def getColorSet(ptree):
    ColorSet = {}
    p1 = re.compile(r'Ansi\s+(\d+)\s+Color')
    for k, v in ptree.items():
        m = p1.match(k)
        key = None
        print('Processing {0}'.format(k))
        if m:
            key = m.group(1)
        else:
            if k in ['Foreground Color', 'Background Color', 'Cursor Color']:
                key = k.split(' ')[0].lower()
        if key:
            r = math.floor(v['Red Component'] * 255)
            g = math.floor(v['Green Component'] * 255)
            b = math.floor(v['Blue Component'] * 255)
            ColorSet[key] = (r, g, b)
    return ColorSet


def readPlist(filename):
    ptree = None
    try:
        with open(filename, 'rb') as fp:
            ptree = plistlib.readPlist(fp)
    except Exception as e:
        print("Couldnot read file: {0}\n{1}".format(filename, e))

    return ptree

if __name__ == '__main__':
    testFN = 'cobalt2.xml'
    ptree = readPlist(testFN)
    print(ptree)
    ColorSet = getColorSet(ptree)
    if ColorSet:
        outFilename = os.path.splitext(testFN)[0]
        writeRoxtermColor(ColorSet, outFilename)
        print('Wrote the Roxterm color scheme to: \n\t{0}'.format(outFilename))
    else:
        print('Could not get color info from iterm color scheme file:\n\t{0}'.format(testFN))
